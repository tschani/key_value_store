﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
using System.Collections;
using System.IO;

namespace Key_Value_Store
{
    [Serializable]
    public class Item
    {
        private String _key, _value;

        public String Key
        {
            get
            {
                return _key;
            }

            set
            {
                _key = value;
            }
        }

        public String Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
            }
        }

        public Item()
        {
            this._key = "";
            this._value = "";
        }

        public Item(String key, String value)
        {
            this._key = key;
            this._value = value;
        }
    }

    public class Program
    {
        private static String fileName = "database.xml";

        public static void Main(string[] args)
        {
            try
            {
                CreateFileIfNotExists();

                String command = args[0];

                switch (command)
                {
                    case "put": PutCommand(args[1], args[2]); break;
                    case "get": GetCommand(args[1]); break;
                    case "find": FindCommand(args[1]); break;
                    case "del": DelCommand(args[1]); break;
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("wrong command!!!\nuse:\n\tkeystore.exe <put> <key> <value>\n\tkeystore.exe <get> <key>\n\tkeystore.exe <find> <searchTerm>\n\tkeystore.exe <del> <key>");
            }
        }

        public static void CreateFileIfNotExists()
        {
            if (!File.Exists(fileName))
                File.Create(fileName);
        }

        //Insert a new Item or change an existing one
        public static void PutCommand(String key, String value)
        {
            List<Item> items = DeserializeItems();

            AddItem(items, key, value);

            WriteToFile(items);
        }

        //Gets an Item by it's key
        public static void GetCommand(String key)
        {
            List<Item> items = DeserializeItems();
            Item item = items.Find(config => config.Key == key);
            if (item != null)
            {
                Console.WriteLine("[" + item.Key + "] => " + item.Value);
            }
            else
            {
                Console.WriteLine("No item with Key ['" + key + "'] found");
            }
        }

        //Gets all Items with a value
        public static void FindCommand(String value)
        {
            List<Item> items = DeserializeItems();
            List<Item> itemsWithValue = items.FindAll(config => config.Value == value);
            if (itemsWithValue != null)
            {
                Console.WriteLine("Found " + itemsWithValue.Count + " items with value ['" + value + "']");

                foreach (Item i in itemsWithValue)
                {
                    Console.WriteLine("[" + i.Key + "] => " + i.Value);
                }
            }
            else
            {
                Console.WriteLine("No items with value ['" + value + "'] found");
            }
        }

        //Deletes an Item by it's key
        public static void DelCommand(String key)
        {
            List<Item> items = DeserializeItems();
            Item itemToDelete = items.Find(config => config.Key == key);

            if (itemToDelete != null)
            {
                items.Remove(itemToDelete);
                SerializeItems(items);
                WriteToFile(items);
            }
            else
            {
                Console.WriteLine("No item with key ['" + key + "'] found");
            }
        }

        //Serializes Items
        public static String SerializeItems(List<Item> items)
        {
            XmlSerializer serialzier = new XmlSerializer(typeof(List<Item>));
            StringWriter writer = new StringWriter();
            XmlSerializerNamespaces nspaces = new XmlSerializerNamespaces();
            nspaces.Add("", "");

            serialzier.Serialize(writer, items, nspaces);

            return writer.ToString();
        }

        //Deserializes Items
        public static List<Item> DeserializeItems()
        {
            try
            {
                String RawData = File.ReadAllText(fileName);

                Dictionary<string, string> collection = new Dictionary<string, string>();

                XmlSerializer xs = new XmlSerializer(typeof(List<Item>));
                StringReader sr = new StringReader(RawData);

                List<Item> items = (List<Item>)xs.Deserialize(sr);

                foreach (Item item in items)
                {
                    collection.Add(item.Key, item.Value);
                }

                return items;
            }
            catch
            {
                return new List<Item>();
            }
        }

        //Adds an Item to the List or changes an existing one
        public static List<Item> AddItem(List<Item> items, string key, string value)
        {
            Item i = items.Find(config => config.Key == key);
            if (i != null && i.Value != value)
                items.Find(config => config.Key == key).Value = value;
            else if (i == null)
                items.Add(new Item(key, value));
            return items;
        }

        public static void WriteToFile(List<Item> items)
        {
            File.WriteAllText(fileName, SerializeItems(items));
        }
    }
}
