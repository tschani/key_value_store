@ECHO OFF
.\keystore put GT610 NVIDIA
.\keystore put GT620 NVIDIA
.\keystore put GT630 NVIDIA
.\keystore put GT640 NVIDIA
.\keystore put GTX650 NVIDIA
.\keystore put GTX660 NVIDIA
.\keystore put GTX670 NVIDIA
.\keystore put GTX680 NVIDIA
.\keystore put GTX690 NVIDIA

.\keystore put GT710 NVIDIA
.\keystore put GT720 NVIDIA
.\keystore put GT730 NVIDIA
.\keystore put GT740 NVIDIA
.\keystore put GTX750 NVIDIA
.\keystore put GTX760 NVIDIA
.\keystore put GTX770 NVIDIA
.\keystore put GTX780 NVIDIA
.\keystore put GTX780Ti NVIDIA

.\keystore put GT910 NVIDIA
.\keystore put GT920 NVIDIA
.\keystore put GT930 NVIDIA
.\keystore put GT940 NVIDIA
.\keystore put GTX950 NVIDIA
.\keystore put GTX960 NVIDIA
.\keystore put GTX970 NVIDIA
.\keystore put GTX980 NVIDIA
.\keystore put GTX980Ti NVIDIA

.\keystore put GT1030 NVIDIA
.\keystore put GT1040 NVIDIA
.\keystore put GTX1050 NVIDIA
.\keystore put GTX1050Ti NVIDIA
.\keystore put GTX1060 NVIDIA
.\keystore put GTX1060Ti NVIDIA
.\keystore put GTX1070 NVIDIA
.\keystore put GTX1070Ti NVIDIA
.\keystore put GTX1080 NVIDIA
.\keystore put GTX1080Ti NVIDIA

.\keystore put RX580 AMD
.\keystore put RX560 AMD
.\keystore put RX480 AMD
.\keystore put R9-295X2 AMD
.\keystore put RX-VEGA AMD

.\keystore find NVIDIA

.\keystore find AMD

PAUSE